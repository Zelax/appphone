package cat.itb.cineapp.ui.Accion;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class AccionViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public AccionViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Categoria: Accion");
    }

    public LiveData<String> getText() {
        return mText;
    }
}