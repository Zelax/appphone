package cat.itb.cineapp.ui.Comedia;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ComediaViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ComediaViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Categoria: Comedia");
    }

    public LiveData<String> getText() {
        return mText;
    }
}