package cat.itb.cineapp.ui.Estrenos;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class EstrenosViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public EstrenosViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Ultimos Estrenos");
    }

    public LiveData<String> getText() {
        return mText;
    }
}