package cat.itb.cineapp.ui.Infantil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class InfantilViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public InfantilViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Categoria: Infantil");
    }

    public LiveData<String> getText() {
        return mText;
    }
}